package com.zilion.simpleapitest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelHelper {

    public ExcelHelper() {
    }

    public ArrayList<Sheet> getSheetListFromFile(String sFullFilePath) {
        ArrayList<Sheet> oSheetList;
        FileInputStream oExcelFile;
        Workbook oWorkbook;
        File oFile;

        oSheetList = new ArrayList<Sheet>();
        try {
            oFile = new File(sFullFilePath);
            oExcelFile = new FileInputStream(oFile);
            oWorkbook = new XSSFWorkbook(oExcelFile);

            /*EJH:BEGIN Loop sheets*/
            Iterator<Sheet> oSheetIterator = oWorkbook.sheetIterator();
            while (oSheetIterator.hasNext()) {
                Sheet oSheet = oSheetIterator.next();
                oSheetList.add(oSheet);
            }
            /*EJH:END Loop sheets*/

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return oSheetList;
    }

    public ArrayList<Row> getRowListFromSheet(Sheet oSheet) {
        ArrayList<Row> oRowList;
        oRowList = new ArrayList<Row>();

        /*EJH:BEGIN Loop rows in sheet*/
        Iterator<Row> oRowIterator = oSheet.iterator();
        while (oRowIterator.hasNext()) {
            Row oCurrentRow = oRowIterator.next();
            oRowList.add(oCurrentRow);
        }
        /*EJH:END Loop rows in sheet*/

        return oRowList;
    }

    public String getCellValueFromRow(Row oRow, int nIndex) {
        String sCellValue = "";
        try {
            sCellValue = getCellValue(oRow.getCell(nIndex));
        } catch (Exception ex) {
        }
        return sCellValue;
    }

    private String getCellValue(Cell oCell) {
        if (oCell == null) {
            return "";
        } else {
            //return oCell.getStringCellValue();
            if (oCell.getCellTypeEnum().compareTo(CellType.NUMERIC) == 0) {
                double nValue = oCell.getNumericCellValue();
                if ((nValue == Math.floor(nValue)) && !Double.isInfinite(nValue)) {
                    return String.valueOf((int) oCell.getNumericCellValue());
                } else {
                    return String.valueOf(oCell.getNumericCellValue());
                }
            } else {
                return oCell.getStringCellValue();
            }/**/
        }
    }
}
