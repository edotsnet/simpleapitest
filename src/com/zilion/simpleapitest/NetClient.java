package com.zilion.simpleapitest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

public class NetClient {

    public NetClient() {
    }

    public String invokeApi(TestCase oTestCase) {
        URL oUrl;
        HttpURLConnection oConnection;
        BufferedReader oBuffered = null;
        StringBuilder oStringBuilder = new StringBuilder();
        String sResponseLine;
        String sResponseBody = "{}";
        try {
            System.out.println("Iniciando invocación API: " + oTestCase.getMethod() + " " + oTestCase.getUrl());
            oUrl = new URL(oTestCase.getUrl());
            oTestCase.setStartDate(new Date());
            //HttpURLConnection.setFollowRedirects(false);
            oConnection = (HttpURLConnection) oUrl.openConnection();
            oConnection.setDoOutput(true);
            oConnection.setConnectTimeout(oTestCase.getTimeout() * 1000); //In millesecond
            oConnection.setReadTimeout(oTestCase.getTimeout() * 1000);
            oConnection.setRequestMethod(oTestCase.getMethod());
            for (Parameter oHeader : oTestCase.getHeaderList()) {
                if (oHeader.getName().length() > 0) {
                    oConnection.setRequestProperty(oHeader.getName(), oHeader.getValue());
                }
            }
            if (oTestCase.getMethod().equals("POST")) {
                OutputStream oOutputStream = oConnection.getOutputStream();
                oOutputStream.write(oTestCase.getBodyRow().getBytes());
                oOutputStream.flush();
            }
            oTestCase.setResponseCode(oConnection.getResponseCode());
            if (oTestCase.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                System.err.println("Failed : HTTP error code : " + oConnection.getResponseCode());
            }

            oBuffered = new BufferedReader(new InputStreamReader((oConnection.getInputStream())));

            try {
                while ((sResponseLine = oBuffered.readLine()) != null) {
                    oStringBuilder.append(sResponseLine).append("\n");
                }
                sResponseBody = oStringBuilder.toString();
            } catch (IOException ex) {
                System.err.println("General Exception: " + ex.getMessage());
                ex.printStackTrace();
            }

            oConnection.disconnect();
        } catch (MalformedURLException ex) {
            System.err.println("Mal Formed URL: " + ex.getMessage());
        } catch (SocketTimeoutException ex) {
            System.err.println("Timeout: " + ex.getMessage());
        } catch (IOException ex) {
            System.err.println("General Exception: " + ex.getMessage());
        } finally {
            oTestCase.setEndDate(new Date());
            System.out.println("Invocación completada.");
        }
        return sResponseBody;
    }

    public static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
}
