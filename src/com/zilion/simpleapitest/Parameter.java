package com.zilion.simpleapitest;

public class Parameter {

    private String name;
    private String value;
    private String shippingIndicator;
    private String replacementIndicator;
    private String sourceScenarioId;
    private String sourceFieldName;
    private String sourceFieldType;

    public Parameter() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the shippingIndicator
     */
    public String getShippingIndicator() {
        return shippingIndicator;
    }

    /**
     * @param shippingIndicator the shippingIndicator to set
     */
    public void setShippingIndicator(String shippingIndicator) {
        this.shippingIndicator = shippingIndicator;
    }

    /**
     * @return the replacementIndicator
     */
    public String getReplacementIndicator() {
        return replacementIndicator;
    }

    /**
     * @param replacementIndicator the replacementIndicator to set
     */
    public void setReplacementIndicator(String replacementIndicator) {
        this.replacementIndicator = replacementIndicator;
    }

    /**
     * @return the sourceScenarioId
     */
    public String getSourceScenarioId() {
        return sourceScenarioId;
    }

    /**
     * @param sourceScenarioId the sourceScenario to set
     */
    public void setSourceScenarioId(String sourceScenarioId) {
        this.sourceScenarioId = sourceScenarioId;
    }

    /**
     * @return the sourceFieldName
     */
    public String getSourceFieldName() {
        return sourceFieldName;
    }

    /**
     * @param sourceFieldName the sourceFieldName to set
     */
    public void setSourceFieldName(String sourceFieldName) {
        this.sourceFieldName = sourceFieldName;
    }

    /**
     * @return the sourceFieldType
     */
    public String getSourceFieldType() {
        return sourceFieldType;
    }

    /**
     * @param sourceFieldType the sourceFieldType to set
     */
    public void setSourceFieldType(String sourceFieldType) {
        this.sourceFieldType = sourceFieldType;
    }
}
