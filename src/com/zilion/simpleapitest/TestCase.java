package com.zilion.simpleapitest;

import java.util.ArrayList;
import java.util.Date;

public class TestCase {
    private String id;
    private String name;
    private String sheetName;
    private String method;
    private String url;
    private int timeout;
    private String bodyRow;
    private String preScenarioId;
    private String postScenarioId;
    private String response;
    private ArrayList<Parameter> headerList;
    private ArrayList<Parameter> fieldList;
    private boolean processed;
    private boolean root;
    private boolean children;
    private Date startDate;
    private Date endDate;
    private int responseCode;
    private long latency;
    
    public TestCase(){
        this.headerList = new ArrayList<Parameter>();
        this.fieldList = new ArrayList<Parameter>();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the sheetName
     */
    public String getSheetName() {
        return sheetName;
    }

    /**
     * @param sheetName the sheetName to set
     */
    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * @return the bodyRow
     */
    public String getBodyRow() {
        return bodyRow;
    }

    /**
     * @param bodyRow the bodyRow to set
     */
    public void setBodyRow(String bodyRow) {
        this.bodyRow = bodyRow;
    }

    /**
     * @return the preScenarioId
     */
    public String getPreScenarioId() {
        return preScenarioId;
    }

    /**
     * @param preScenarioId the preScenarioId to set
     */
    public void setPreScenarioId(String preScenarioId) {
        this.preScenarioId = preScenarioId;
    }

    /**
     * @return the postScenarioId
     */
    public String getPostScenarioId() {
        return postScenarioId;
    }

    /**
     * @param postScenarioId the postScenarioId to set
     */
    public void setPostScenarioId(String postScenarioId) {
        this.postScenarioId = postScenarioId;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the latency
     */
    public long getLatency() {
        return  (this.endDate.getTime() - this.startDate.getTime());
        //return latency;
    }

    /**
     * @param latency the latency to set
     */
    public void setLatency(long latency) {
        this.latency = latency;
    }

    /**
     * @return the headerList
     */
    public ArrayList<Parameter> getHeaderList() {
        return headerList;
    }

    /**
     * @param headerList the headerList to set
     */
    public void setHeaderList(ArrayList<Parameter> headerList) {
        this.headerList = headerList;
    }

    /**
     * @return the fieldList
     */
    public ArrayList<Parameter> getFieldList() {
        return fieldList;
    }

    /**
     * @param fieldList the fieldList to set
     */
    public void setFieldList(ArrayList<Parameter> fieldList) {
        this.fieldList = fieldList;
    }

    /**
     * @return the processed
     */
    public boolean isProcessed() {
        return processed;
    }

    /**
     * @param processed the processed to set
     */
    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    /**
     * @return the root
     */
    public boolean isRoot() {
        return root;
    }

    /**
     * @param root the root to set
     */
    public void setRoot(boolean root) {
        this.root = root;
    }

    /**
     * @return the children
     */
    public boolean hasChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(boolean children) {
        this.children = children;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
