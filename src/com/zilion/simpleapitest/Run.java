package com.zilion.simpleapitest;

import com.zilion.simpleapitest.ui.MainJFrame;
import com.zilion.util.ZFile;
import com.zilion.util.ZText;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class Run {

    private MainJFrame frmMain;

    public Run() {
    }

    public Run(final MainJFrame frmMain) {
        this.frmMain = frmMain;
    }

    public static void main(String[] args) {
        String sFilePathTestCase;
        String sResponseDirPath;

        if (args.length == 2) {
            sFilePathTestCase = args[0];
            sResponseDirPath = args[1];
        } else {
            sFilePathTestCase = "D:\\Users\\s52066\\Documents\\App portables\\SimpleApiTest\\SimpleApiTest-SOAT.xlsx";
            sResponseDirPath = "D:\\Users\\s52066\\Documents\\App portables\\SimpleApiTest";
            //throw new RuntimeException("Failed : Error en parametros de entrada." + ZText.LINE_SEPARATOR + " Parametro 1: Ruta completa del archivo. Ejm: C:\\carpeta\\casos.xlsx" + ZText.LINE_SEPARATOR + " Parametro 2: Ruta completa de la carpeta de respuesta. Ejm: C:\\CarpetaRespuesta");
        }

        Run oRun = new Run();
        oRun.init(sFilePathTestCase, sResponseDirPath);
    }

    public void init(String sFilePathTestCase, String sResponseDirPath) {
        String sColumnA, sColumnB, sColumnC, sColumnD, sColumnE, sColumnF, sColumnG, sColumnH;

        boolean lIsHeader = false;
        boolean lIsField = false;

        NetClient.disableSslVerification(); //TODO:DELETE

        ExcelHelper oExcel = new ExcelHelper();
        RunTest oRunTest = new RunTest(this.frmMain);

        ArrayList<Sheet> oSheetList = oExcel.getSheetListFromFile(sFilePathTestCase);
        ArrayList<TestCase> oTestCaseList = new ArrayList<TestCase>();
        for (Sheet oSheet : oSheetList) {
            TestCase oTestCase = new TestCase();

            oTestCase.setSheetName(oSheet.getSheetName());
            ArrayList<Row> oRowList = oExcel.getRowListFromSheet(oSheet);
            for (Row oRow : oRowList) {
                sColumnA = oExcel.getCellValueFromRow(oRow, 0);
                sColumnB = oExcel.getCellValueFromRow(oRow, 1);
                sColumnC = oExcel.getCellValueFromRow(oRow, 2);
                sColumnD = oExcel.getCellValueFromRow(oRow, 3);
                sColumnE = oExcel.getCellValueFromRow(oRow, 4);
                sColumnF = oExcel.getCellValueFromRow(oRow, 5);
                sColumnG = oExcel.getCellValueFromRow(oRow, 6);
                sColumnH = oExcel.getCellValueFromRow(oRow, 7);

                if (sColumnA.equals("ESCENARIO ID")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setId(sColumnB);
                } else if (sColumnA.equals("ESCENARIO")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setName(sColumnB);
                } else if (sColumnA.equals("METODO")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setMethod(sColumnB);
                } else if (sColumnA.equals("URL")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setUrl(sColumnB);
                } else if (sColumnA.equals("TIMEOUT")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setTimeout(Integer.parseInt(sColumnB));
                } else if (sColumnA.equals("HEADERS:")) {
                    lIsHeader = true;
                    lIsField = false;
                } else if (sColumnA.equals("BODY-ROW")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setBodyRow(sColumnB);
                } else if (sColumnA.equals("PRE-ESCENARIO ID")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setPreScenarioId(sColumnB);
                } else if (sColumnA.equals("POST-ESCENARIO ID")) {
                    lIsHeader = false;
                    lIsField = false;
                    oTestCase.setPostScenarioId(sColumnB);
                } else if (sColumnA.equals("BODY REEPLACE")) {
                    lIsHeader = false;
                    lIsField = true;
                } else if (lIsHeader) {
                    if (sColumnB.trim().length() > 0) {
                        Parameter oParameter = new Parameter();
                        oParameter.setName(sColumnB);
                        oParameter.setValue(sColumnC);
                        oTestCase.getHeaderList().add(oParameter);
                    }
                } else if (lIsField) {
                    if (sColumnB.trim().length() > 0) {
                        Parameter oParameter = new Parameter();
                        oParameter.setName(sColumnB);
                        oParameter.setValue(sColumnC);
                        oParameter.setShippingIndicator(sColumnD);
                        oParameter.setReplacementIndicator(sColumnE);
                        oParameter.setSourceScenarioId(sColumnF);
                        oParameter.setSourceFieldName(sColumnG);
                        oParameter.setSourceFieldType(sColumnH);
                        oTestCase.getFieldList().add(oParameter);
                    }
                }
            }
            if (oTestCase.getPreScenarioId().length() > 0) {
                oTestCase.setRoot(false);
            } else {
                oTestCase.setRoot(true);
            }
            oTestCaseList.add(oTestCase);
        }

        oRunTest.invoke(oTestCaseList, null);

        for (TestCase oTestCaseResult : oRunTest.getTestCaseResultList()) {
            String sJsonFormatted = JsonHelper.getJsonFormatted(oTestCaseResult.getBodyRow());
            String sFileName = sResponseDirPath + "\\" + oTestCaseResult.getSheetName() + "_Result.log";
            String sContent = "";
            sContent = sContent + "====================================================================" + ZText.LINE_SEPARATOR;
            sContent = sContent + " ESCENARIO: " + oTestCaseResult.getId() + " - " + oTestCaseResult.getName() + ZText.LINE_SEPARATOR;
            sContent = sContent + "====================================================================" + ZText.LINE_SEPARATOR + ZText.LINE_SEPARATOR;
            sContent = sContent + "======================== REQUEST ======================== " + ZText.LINE_SEPARATOR;
            sContent = sContent + oTestCaseResult.getMethod() + "  " + oTestCaseResult.getUrl() + ZText.LINE_SEPARATOR;
            sContent = sContent + "Status: " + oTestCaseResult.getResponseCode() + "  Time: " + oTestCaseResult.getLatency() + ZText.LINE_SEPARATOR;
            for (Parameter oParameter : oTestCaseResult.getHeaderList()) {
                sContent = sContent + oParameter.getName() + " : " + oParameter.getValue() + ZText.LINE_SEPARATOR;
            }
            sContent = sContent + "Request body:" + ZText.LINE_SEPARATOR + ZText.LINE_SEPARATOR;
            ZFile.write(sContent, sFileName, false);
            ZFile.write(sJsonFormatted, sFileName, true);

            sJsonFormatted = JsonHelper.getJsonFormatted(oTestCaseResult.getResponse());
            sContent = ZText.LINE_SEPARATOR + "======================== RESPONSE ========================" + ZText.LINE_SEPARATOR;
            sContent = sContent + "Response body:" + ZText.LINE_SEPARATOR + ZText.LINE_SEPARATOR;
            ZFile.write(sContent, sFileName, true);
            ZFile.write(sJsonFormatted, sFileName, true);
        }
    }
}

/*














 */
