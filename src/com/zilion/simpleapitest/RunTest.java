package com.zilion.simpleapitest;

import com.fasterxml.jackson.databind.JsonNode;
import com.zilion.simpleapitest.ui.MainJFrame;
import java.util.ArrayList;

public class RunTest {

    private MainJFrame frmMain;
    private ArrayList<TestCase> testCaseResultList;

    public RunTest() {
    }

    public RunTest(final MainJFrame frmMain) {
        this.testCaseResultList = new ArrayList<TestCase>();
        this.frmMain = frmMain;
    }

    /**
     * @return the oTestCaseResultList
     */
    public ArrayList<TestCase> getTestCaseResultList() {
        return testCaseResultList;
    }

    /**
     * @param testCaseResultList the oTestCaseResultList to set
     */
    public void setTestCaseResultList(ArrayList<TestCase> testCaseResultList) {
        this.testCaseResultList = testCaseResultList;
    }

    public TestCase getTestCaseResultById(String id) {
        TestCase oTestCaseResult = null;
        for (TestCase oTestCase : this.testCaseResultList) {
            if (oTestCase.getId().equals(id)) {
                oTestCaseResult = oTestCase;
                break;
            }
        }
        return oTestCaseResult;
    }

    public void invoke(ArrayList<TestCase> oTestCaseList, TestCase oTestCaseParent) {
        for (TestCase oTestCaseTemp : oTestCaseList) {
            //System.out.printf("Iniciando escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
            //TODO:ERROR:System.console().writer().println("Iniciando escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
            if (oTestCaseParent == null) {
                if (oTestCaseTemp.isRoot()) {
                    System.out.println("Iniciando escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
                    this.frmMain.printLog("Iniciando escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
                    try {
                        this.getTestCaseResultList().add(getTestCaseProcessed(oTestCaseTemp));
                        invoke(oTestCaseList, oTestCaseTemp);
                    } catch (Exception ex) {
                        //throw new RuntimeException("Error al ejecutar el escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName() + ": " + ex.getMessage());
                        System.err.println("Error al ejecutar el escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName() + ": " + ex.getMessage());
                    }
                    System.out.println("Saliendo del escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
                }
            } else {
                if (oTestCaseTemp.getPreScenarioId().equals(oTestCaseParent.getId())) {
                    System.out.println("Iniciando escenario hijo " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
                    try {
                        this.getTestCaseResultList().add(getTestCaseProcessed(oTestCaseTemp));
                        invoke(oTestCaseList, oTestCaseTemp);
                    } catch (Exception ex) {
                        //throw new RuntimeException("Error al ejecutar el escenario " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName() + ": " + ex.getMessage());
                        System.err.println("Error al ejecutar el escenario hijo " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName() + ": " + ex.getMessage());
                    }
                    System.out.println("Saliendo del escenario hijo " + oTestCaseTemp.getId() + " - " + oTestCaseTemp.getName());
                }
            }
        };
    }

    private TestCase getTestCaseProcessed(TestCase oTestCase) {
        NetClient oNetClient = new NetClient();
        JsonNode oJsonTreeNode = JsonHelper.getJsonTreeNode(oTestCase.getBodyRow());
        for (Parameter oParam : oTestCase.getFieldList()) {
            String sValue = oParam.getValue();
            if (oParam.getSourceScenarioId().length() > 0) {
                if (oParam.getSourceFieldType().equals("RESPONSE")) {
                    TestCase oTestCaseParent = getTestCaseResultById(oParam.getSourceScenarioId());
                    JsonNode oJsonParent = JsonHelper.getJsonTreeNode(oTestCaseParent.getResponse());
                    sValue = JsonHelper.getValue(oJsonParent, oParam.getSourceFieldName());
                }
            }
            try {
                oJsonTreeNode = JsonHelper.getNodeUpdated(oJsonTreeNode, oParam.getName(), sValue);
            } catch (Exception ex) {
                throw new RuntimeException("Error en la estructura JSON del request body para el parametro \"" + oParam.getName() + "\" : " + ex.getMessage());
            }
        }
        oTestCase.setBodyRow(oJsonTreeNode.toString());
        oTestCase.setResponse(oNetClient.invokeApi(oTestCase));

        return oTestCase;
    }
}
