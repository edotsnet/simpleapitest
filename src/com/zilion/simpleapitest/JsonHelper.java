/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zilion.simpleapitest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Personal
 */
public class JsonHelper {

    public JsonHelper() {
    }

    public static JsonNode getJsonTreeNode(String sJson) {
        ObjectMapper oMapper = new ObjectMapper();
        JsonNode oJsonObject = null;
        try {
            oJsonObject = oMapper.readTree(sJson);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return oJsonObject;
    }

    public static String getJsonFormatted(String sJsonString) {
        String sJsonFormatted = "";
        try {
            ObjectMapper oObjectMapper = new ObjectMapper();
            Object oJson = oObjectMapper.readValue(sJsonString, Object.class);
            sJsonFormatted = oObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(oJson);
        } catch (IOException ex) {

        }
        return sJsonFormatted;
    }

    public static JsonNode getNodeUpdated(JsonNode oJsonParent, String sFieldPath, String sNewValue) {
        JsonNode oLocatedNode;
        JsonNode oNodeChanged;
        JsonNode oNodeLast;
        String[] sFieldNameArray;
        String sNewFieldPath;
        String sFieldName;
        int nIndex, nCurrentIndex;

        oNodeChanged = null;
        oNodeLast = null;
        sFieldNameArray = sFieldPath.split("=>");
        sNewFieldPath = sFieldPath.substring(sFieldNameArray[0].length()).replaceFirst("=>", "");
        if (isArray(sFieldNameArray[0])) {
            sFieldName = getFieldName(sFieldNameArray[0]);
            nCurrentIndex = getIndex(sFieldNameArray[0]);
        } else {
            sFieldName = sFieldNameArray[0];
            nCurrentIndex = 0;
        }

        if (sFieldNameArray.length == 1) {
            ((ObjectNode) oJsonParent).put(sFieldName, sNewValue);
        } else {
            oLocatedNode = oJsonParent.path(sFieldName);
            if (oLocatedNode.isArray()) {
                nIndex = 0;
                for (JsonNode oCurrentNode : oLocatedNode) {
                    oNodeLast = oCurrentNode;
                    if (nIndex == nCurrentIndex) {
                        oNodeChanged = getNodeUpdated(oCurrentNode, sNewFieldPath, sNewValue);
                        ((ArrayNode) oJsonParent.withArray(sFieldName)).set(0, oNodeChanged);
                    }
                    nIndex++;
                }
                if (nIndex > 0 && nIndex < nCurrentIndex + 1) {
                    oNodeChanged = getNodeUpdated(oNodeLast, sNewFieldPath, sNewValue);
                    ((ArrayNode) oJsonParent.withArray(sFieldName)).insert(nIndex, oNodeChanged);
                }
            } else {
                oNodeChanged = getNodeUpdated(oLocatedNode, sNewFieldPath, sNewValue);
                ((ObjectNode) oJsonParent).set(sFieldName, oNodeChanged);
            }
        }
        return oJsonParent;
    }

    public static String getValue(JsonNode oJsonParent, String sFieldPath) {
        String sValue;
        JsonNode oLocatedNode;
        JsonNode oNodeChanged;
        JsonNode oNodeLast;
        String[] sFieldNameArray;
        String sNewFieldPath;
        String sFieldName;
        int nIndex, nCurrentIndex;

        sValue = "";
        oNodeChanged = null;
        oNodeLast = null;
        sFieldNameArray = sFieldPath.split("=>");
        sNewFieldPath = sFieldPath.substring(sFieldNameArray[0].length()).replaceFirst("=>", "");
        if (isArray(sFieldNameArray[0])) {
            sFieldName = getFieldName(sFieldNameArray[0]);
            nCurrentIndex = getIndex(sFieldNameArray[0]);
        } else {
            sFieldName = sFieldNameArray[0];
            nCurrentIndex = 0;
        }

        if (sFieldNameArray.length == 1) {
            //((ObjectNode) oJsonParent).put(sFieldName, sNewValue);}
            sValue = oJsonParent.path(sFieldName).textValue();
        } else {
            oLocatedNode = oJsonParent.path(sFieldName);
            if (oLocatedNode.isArray()) {
                nIndex = 0;
                for (JsonNode oCurrentNode : oLocatedNode) {
                    oNodeLast = oCurrentNode;
                    if (nIndex == nCurrentIndex) {
                        sValue = getValue(oCurrentNode, sNewFieldPath);
                        //oNodeChanged = getNodeUpdated(oCurrentNode, sNewFieldPath, sNewValue);
                        //((ArrayNode) oJsonParent.withArray(sFieldName)).set(0, oNodeChanged);

                    }
                    nIndex++;
                }
                if (nIndex > 0 && nIndex < nCurrentIndex + 1) {
                    //ged = getNodeUpdated(oNodeLast, sNewFieldPath, sNewValue);
                    //((ArrayNode) oJsonParent.withArray(sFieldName)).insert(nIndex, oNodeChanged);
                }
            } else {
                sValue = getValue(oLocatedNode, sNewFieldPath);
                //oNodeChanged = getNodeUpdated(oLocatedNode, sNewFieldPath, sNewValue);
                //((ObjectNode) oJsonParent).set(sFieldName, oNodeChanged);
            }
        }
        return sValue;
    }

    public static boolean isArray(String sFieldName) {
        boolean lResult = false;
        if (sFieldName.contains("[")) {
            lResult = true;
        }
        return lResult;
    }

    public static String getFieldName(String sFieldName) {
        String[] sResult = sFieldName.replace("[", ",").split(",");
        return sResult[0];
    }

    public static int getIndex(String sFieldName) {
        String[] sResult = sFieldName.replace("]", "").replace("[", ",").split(",");
        return Integer.parseInt(sResult[1]);
    }
}
